FROM python:3.11.3
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install prometheus-client
COPY app.py .
EXPOSE 8000
CMD ["python", "app.py"]
